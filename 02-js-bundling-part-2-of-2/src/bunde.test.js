const fs = require('fs')
const path = require('path')
const bundle = require('./bundle')

describe('bundle', () => {
  beforeAll(() => {
    if (!fs.existsSync(outputDirPath)) {
      fs.mkdirSync(outputDirPath)
    }
  })
  const outputDirPath = path.join(__dirname, '.tmp')
  describe('given an entry point and an output folder', () => {
    it('writes a bundle into the output folder', () => {
      bundle({
        entryFile: './test-modules/module-c.js',
        entryFilePath: __dirname,
        outputFolder: outputDirPath,
      })
      const bundledCode = fs.readFileSync(
        path.join(outputDirPath, 'bundle.js'),
        {
          encoding: 'utf-8',
        },
      )
      expect(bundledCode).toMatchInlineSnapshot(`
"const modules = { '/home/lachlan/git/brain-school/02-js-bundling-part-2-of-2/src/test-modules/module-c.js': function (exports,require){ const _imported = require("/home/lachlan/git/brain-school/02-js-bundling-part-2-of-2/src/test-modules/folder-a/module-a.js");
const z = 'luluLimen';
exports.z = z;
console.log(\`So yeah, \${_imported["default"]} \${z}\`); },'/home/lachlan/git/brain-school/02-js-bundling-part-2-of-2/src/test-modules/folder-a/module-a.js': function (exports,require){ const _imported = require("/home/lachlan/git/brain-school/02-js-bundling-part-2-of-2/src/test-modules/folder-a/folder-b/module-b.js");
const a = "hey " + _imported["default"];
exports.default = a; },'/home/lachlan/git/brain-school/02-js-bundling-part-2-of-2/src/test-modules/folder-a/folder-b/module-b.js': function (exports,require){ const b = "bebe";
exports.default = b; } };
const entry = "/home/lachlan/git/brain-school/02-js-bundling-part-2-of-2/src/test-modules/module-c.js";
function webpackStart({modules,entry}){
const moduleCache = {};
//Webpack require function
const require = moduleName => {
//If the module is cached, return the cached version
if(moduleCache[moduleName]){
return moduleCache[moduleName];
}
const exports = {};
//We need to avoid the cyclical dependencies
//when invoking require()
moduleCache[moduleName] = exports;
//require() the module 
modules[moduleName](exports,require);
return moduleCache[moduleName];
};
//Execute the program
require(entry);
}
webpackStart({modules,entry});
"
`)
    })
  })
  describe('given modules with circular dependencies', () => {
    it('writes a bundle into the output folder', () => {
      bundle({
        entryFile: './test-modules-circular/circular-entry.js',
        entryFilePath: __dirname,
        outputFolder: outputDirPath,
        outputName: 'circular-bundle.js',
      })
      const bundledCode = fs.readFileSync(
        path.join(outputDirPath, 'circular-bundle.js'),
        {
          encoding: 'utf-8',
        },
      )
      expect(bundledCode).toMatchInlineSnapshot(`
"const modules = { '/home/lachlan/git/brain-school/02-js-bundling-part-2-of-2/src/test-modules-circular/circular-entry.js': function (exports,require){ const _imported = require("/home/lachlan/git/brain-school/02-js-bundling-part-2-of-2/src/test-modules-circular/circular-dep.js");
const myVar = 3;
// Need to export before we call calc
exports.default = myVar;
console.log(\`Dep calc is \${_imported["default"]()}\`); },'/home/lachlan/git/brain-school/02-js-bundling-part-2-of-2/src/test-modules-circular/circular-dep.js': function (exports,require){ const _imported = require("/home/lachlan/git/brain-school/02-js-bundling-part-2-of-2/src/test-modules-circular/circular-entry.js");
const calc = () => _imported["default"] + 1;
exports.default = calc; } };
const entry = "/home/lachlan/git/brain-school/02-js-bundling-part-2-of-2/src/test-modules-circular/circular-entry.js";
function webpackStart({modules,entry}){
const moduleCache = {};
//Webpack require function
const require = moduleName => {
//If the module is cached, return the cached version
if(moduleCache[moduleName]){
return moduleCache[moduleName];
}
const exports = {};
//We need to avoid the cyclical dependencies
//when invoking require()
moduleCache[moduleName] = exports;
//require() the module 
modules[moduleName](exports,require);
return moduleCache[moduleName];
};
//Execute the program
require(entry);
}
webpackStart({modules,entry});
"
`)
    })
  })
})
