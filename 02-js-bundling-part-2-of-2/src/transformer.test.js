const transformer = require('./transformer.js')
const createTransformer = require('./transformer.js').createTransformer
const prettier = require('prettier')
// TODO tests for reassignment
const assertCodeTransformedCorrectly = (
  code,
  transformedCode,
  customTransformer = transformer,
) =>
  expect(prettier.format(customTransformer(code), { parser: 'babel' })).toEqual(
    prettier.format(transformedCode, { parser: 'babel' }),
  )
describe('transformer.js', () => {
  describe('import statements', () => {
    describe('default imports', () => {
      it('transforms a default import with no references', () => {
        const code = `import a from './module-a.js'`
        const transformedCode = `const _imported = require('./module-a.js');`
        assertCodeTransformedCorrectly(code, transformedCode)
      })
      it('transforms a default import with a reference', () => {
        const code = `import a from './module-a.js'; 
          console.log(a)`
        const transformedCode = `const _imported = require('./module-a.js'); 
          console.log(_imported["default"])`
        assertCodeTransformedCorrectly(code, transformedCode)
      })
    })
    describe('non-default imports', () => {
      it('transforms a non-default import with no references', () => {
        const code = `import {a} from './module-a.js'; 
          var test = a`
        const transformedCode = `const _imported = require('./module-a.js'); 
          var test = _imported["a"]`
        assertCodeTransformedCorrectly(code, transformedCode)
      })
      it('transforms a non-default import with a reference', () => {
        const code = `import {a} from './module-a.js'; 
          console.log(a)`
        const transformedCode = `const _imported = require('./module-a.js'); 
          console.log(_imported["a"])`
        assertCodeTransformedCorrectly(code, transformedCode)
      })
      it('transforms multiple non-default imports with no references', () => {
        const code = `import {a,b} from './module-a.js'`
        const transformedCode = `const _imported = require('./module-a.js')`
        assertCodeTransformedCorrectly(code, transformedCode)
      })
      it('transforms multiple non-default imports with references', () => {
        const code = `import {a,b} from './module-a.js'; 
          console.log(a)
          console.log(b)`
        const transformedCode = `const _imported = require('./module-a.js'); 
        console.log(_imported["a"])
        console.log(_imported["b"])`
        assertCodeTransformedCorrectly(code, transformedCode)
      })
    })
    describe('default and non-default imports', () => {
      it('transforms default and non-default imports with no references', () => {
        const code = `import a,{b,c} from './module-a.js'`
        const transformedCode = `const _imported = require('./module-a.js')`
        assertCodeTransformedCorrectly(code, transformedCode)
      })
      it('transforms default and non-default imports with references', () => {
        const code = `import a,{b,c} from './module-a.js'; 
          var test = a, test2 = b, test3 = c`
        const transformedCode = `const _imported = require('./module-a.js'); 
          var test = _imported["default"], test2 = _imported["b"], test3 = _imported["c"]`
        assertCodeTransformedCorrectly(code, transformedCode)
      })
    })
    describe('Renaming imports', () => {
      it('transforms default and non-default imports with no references', () => {
        const code = `import a, {b as z, c as x} from './module-a.js'`
        const transformedCode = `const _imported = require('./module-a.js')`
        assertCodeTransformedCorrectly(code, transformedCode)
      })
      it('transforms default and non-default imports with references', () => {
        const code = `import a, {b as z, c as x} from './module-a.js'; 
          var test = a, test2 = z, test3 = x`
        const transformedCode = `const _imported = require('./module-a.js'); 
          var test = _imported["default"], test2 = _imported["b"], test3 = _imported["c"]`
        assertCodeTransformedCorrectly(code, transformedCode)
      })
    })
    describe('Multiple imports', () => {
      describe('of the same module', () => {
        it(`creates two import objects`, () => {
          const code = `import a from './module-a.js'
            import b from './module-a.js'`
          const transformedCode = `const _imported = require('./module-a.js')
            const _imported2 = require('./module-a.js')`
          assertCodeTransformedCorrectly(code, transformedCode)
        })
      })
      describe('of different modules', () => {
        it(`creates two import objects`, () => {
          const code = `import a from './module-a.js'
            import b from './module-b.js'`
          const transformedCode = `const _imported = require('./module-a.js')
            const _imported2 = require('./module-b.js')`
          assertCodeTransformedCorrectly(code, transformedCode)
        })
      })
    })
    describe('path resolution', () => {
      it('rewrites module paths using a custom path resolver', () => {
        const code =
          //
          `
          import a from './module-a.js';
          import c,{d} from './module-b.js';`
        const transformedCode =
          //
          `
          const _imported = require('/absolute/path/to/module-a.js');
          const _imported2 = require('/absolute-path-to/module-b.js')`
        const pathResolutionMap = {
          './module-a.js': '/absolute/path/to/module-a.js',
          './module-b.js': '/absolute-path-to/module-b.js',
        }
        const customTransformer = createTransformer(
          (path) => pathResolutionMap[path],
        )
        assertCodeTransformedCorrectly(code, transformedCode, customTransformer)
      })
    })
  })
  describe('export statements', () => {
    describe('default export', () => {
      it('transforms a default export', () => {
        const code = `export default a;`
        const transformedCode = `exports.default = a;`
        assertCodeTransformedCorrectly(code, transformedCode)
      })
    })
    describe('non-default exports', () => {
      // TODO anonymous exports
      it('transforms a non-default class declaration', () => {
        const code = `export class FooBaz{}`
        const transformedCode = `class FooBaz{}; exports.FooBaz = FooBaz`
        assertCodeTransformedCorrectly(code, transformedCode)
      })
      it('transforms a non-default function declaration', () => {
        const code = `export function flimFlam(){};`
        const transformedCode = `function flimFlam(){}; exports.flimFlam = flimFlam;`
        assertCodeTransformedCorrectly(code, transformedCode)
      })
      it('transforms non-default variable declarations', () => {
        const code = `export const zoomZoom = 3, flurpDerp = "simsim";`
        const transformedCode = `const zoomZoom = 3, flurpDerp = "simsim"; exports.zoomZoom = zoomZoom; exports.flurpDerp = flurpDerp;`
        assertCodeTransformedCorrectly(code, transformedCode)
      })
    })
  })
})
