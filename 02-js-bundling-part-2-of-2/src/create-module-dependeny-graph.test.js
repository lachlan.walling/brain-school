const createModuleDependencyGraph = require('./create-module-dependency-graph.js')
const path = require('path')

const expectGraphNode = (path, dependencies) =>
  expect.objectContaining({
    filePath: path,
    dependencies: expect.arrayContaining(dependencies),
  })

describe('create-module-dependency-graph', () => {
  describe('given a single module: A', () => {
    it('returns a graph with a single node', () => {
      const relativePathToModuleB =
        './test-modules/folder-a/folder-b/module-b.js'
      const absolutePathToModuleB = path.join(
        __dirname,
        './test-modules/folder-a/folder-b/module-b.js',
      )
      expect(
        createModuleDependencyGraph(__dirname, relativePathToModuleB),
      ).toEqual(
        expect.objectContaining({
          [absolutePathToModuleB]: expectGraphNode(absolutePathToModuleB, []),
        }),
      )
    })
  })
  describe('given 2 modules: B<--depends-on--A', () => {
    it('returns a graph with two nodes', () => {
      const relativePathToModuleA = './test-modules/folder-a/module-a.js'
      const absolutePathToModuleA = path.join(
        __dirname,
        './test-modules/folder-a/module-a.js',
      )
      const absolutePathToModuleB = path.join(
        __dirname,
        './test-modules/folder-a',
        './folder-b/module-b.js',
      )
      expect(
        createModuleDependencyGraph(__dirname, relativePathToModuleA),
      ).toEqual(
        expect.objectContaining({
          [absolutePathToModuleA]: expectGraphNode(absolutePathToModuleA, [
            absolutePathToModuleB,
          ]),
          [absolutePathToModuleB]: expectGraphNode(absolutePathToModuleB, []),
        }),
      )
    })
  })

  describe('given 3 modules: C<--depends-on--B<--depends-on--A', () => {
    it('returns a graph with three nodes', () => {
      const relativePathToModuleC = './test-modules/module-c.js'
      const absolutePathToModuleC = path.join(
        __dirname,
        './test-modules/module-c.js',
      )
      const absolutePathToModuleA = path.join(
        __dirname,
        './test-modules/',
        './folder-a/module-a.js',
      )
      const absolutePathToModuleB = path.join(
        __dirname,
        './test-modules/folder-a',
        './folder-b/module-b.js',
      )
      expect(
        createModuleDependencyGraph(__dirname, relativePathToModuleC),
      ).toEqual(
        expect.objectContaining({
          [absolutePathToModuleC]: expectGraphNode(absolutePathToModuleC, [
            absolutePathToModuleA,
          ]),
          [absolutePathToModuleA]: expectGraphNode(absolutePathToModuleA, [
            absolutePathToModuleB,
          ]),
          [absolutePathToModuleB]: expectGraphNode(absolutePathToModuleB, []),
        }),
      )
    })
  })
  describe('given 1 module which depends on itself', () => {
    it('returns a graph with one node', () => {
      const relativePathToSelfDep = './test-modules-circular/self-dep.js'
      const absolutePathToSelfDep = path.join(__dirname, relativePathToSelfDep)
      expect(
        createModuleDependencyGraph(__dirname, relativePathToSelfDep),
      ).toEqual(
        expect.objectContaining({
          [absolutePathToSelfDep]: expectGraphNode(absolutePathToSelfDep, [
            absolutePathToSelfDep,
          ]),
        }),
      )
    })
  })
  describe('given 2 modules which depends on each other', () => {
    it('returns a graph with two nodes', () => {
      const relativePathToEntry = './test-modules-circular/circular-entry.js'
      const relativePathToDep = './test-modules-circular/circular-dep.js'
      const absolutePathToEntry = path.join(__dirname, relativePathToEntry)
      const absolutePathToDep = path.join(__dirname, relativePathToDep)
      expect(
        createModuleDependencyGraph(__dirname, relativePathToEntry),
      ).toEqual(
        expect.objectContaining({
          [absolutePathToEntry]: expectGraphNode(absolutePathToEntry, [
            absolutePathToDep,
          ]),
          [absolutePathToDep]: expectGraphNode(absolutePathToDep, [
            absolutePathToEntry,
          ]),
        }),
      )
    })
  })
})
