import myVar from './circular-entry.js'

const calc = () => myVar + 1

export default calc
