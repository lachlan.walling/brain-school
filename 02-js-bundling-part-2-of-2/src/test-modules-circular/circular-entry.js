import calc from './circular-dep.js'

const myVar = 3
// Need to export before we call calc
export default myVar

console.log(`Dep calc is ${calc()}`)
