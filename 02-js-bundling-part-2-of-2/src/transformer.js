const babel = require('@babel/core')

const getImportedName = (specifier) => {
  if (specifier.isImportDefaultSpecifier()) {
    return 'default'
  } else {
    return specifier.get('imported').node.name
  }
}

const createDeclarationFromRequire = (types, name, modulePath) =>
  types.variableDeclaration('const', [
    types.variableDeclarator(
      name,
      types.callExpression(types.identifier('require'), [
        types.stringLiteral(modulePath),
      ]),
    ),
  ])

const replaceImportDeclarationWithCJS = (types, pathResolver, path) => {
  const specifiers = path.get('specifiers')
  const modulePath = pathResolver(path.get('source.value').node)

  const importObjectName = path.scope.generateUidIdentifier('imported')
  path.replaceWith(
    createDeclarationFromRequire(types, importObjectName, modulePath),
  )

  specifiers.forEach((specifier) =>
    specifier.scope
      .getBinding(specifier.get('local').node.name)
      .referencePaths.forEach((path) => {
        path.replaceWith(
          types.memberExpression(
            importObjectName,
            types.stringLiteral(getImportedName(specifier)),
            true,
          ),
        )
      }),
  )
}

const createDefaultExportAssignment = (types, value) =>
  types.expressionStatement(
    types.assignmentExpression(
      '=',
      types.memberExpression(
        types.identifier('exports'),
        types.identifier('default'),
      ),
      value,
    ),
  )

const replaceDefaultExportDeclarationWithCJS = (types, path) => {
  const declaration = path.get('declaration')
  path.replaceWith(
    createDefaultExportAssignment(types, types.toExpression(declaration.node)),
  )
}

const createExportAssignment = (types, name, value) =>
  types.expressionStatement(
    types.assignmentExpression(
      '=',
      types.memberExpression(types.identifier('exports'), name),
      value,
    ),
  )

const replaceExportVariableDeclarationsWithCJS = (types, path) => {
  const declaration = path.get('declaration')
  path.replaceWithMultiple([
    declaration.node,
    ...declaration.get('declarations').map((variableDeclaration) => {
      const name = variableDeclaration.get('id').node
      return createExportAssignment(types, name, name)
    }),
  ])
}

const replaceExportClassAndFunctionDeclarationsWithCJS = (types, path) => {
  const declaration = path.get('declaration')
  const name = declaration.get('id').node
  path.replaceWithMultiple([
    declaration.node,
    createExportAssignment(types, name, name),
  ])
}

const replaceExportDeclarationWithCJS = (types, path) => {
  const declaration = path.get('declaration')
  if (declaration.isClassDeclaration() || declaration.isFunctionDeclaration()) {
    replaceExportClassAndFunctionDeclarationsWithCJS(types, path)
  } else if (declaration.isVariableDeclaration()) {
    replaceExportVariableDeclarationsWithCJS(types, path)
  }
}

const createTransformer = (pathResolver) => (code) => {
  const { code: transformedCode } = babel.transformSync(code, {
    plugins: [
      function transformerPlugin({ types }) {
        return {
          visitor: {
            ImportDeclaration(path) {
              replaceImportDeclarationWithCJS(types, pathResolver, path)
            },
            ExportDefaultDeclaration(path) {
              replaceDefaultExportDeclarationWithCJS(types, path)
            },
            ExportNamedDeclaration(path) {
              replaceExportDeclarationWithCJS(types, path)
            },
          },
        }
      },
    ],
  })
  return transformedCode
}

module.exports = createTransformer((path) => path)

module.exports.createTransformer = createTransformer
