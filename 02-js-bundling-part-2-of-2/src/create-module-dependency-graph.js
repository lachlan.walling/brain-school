const path = require('path')
const babel = require('@babel/core')
const fs = require('fs')
const createTransformer = require('./transformer.js').createTransformer
const createFilePathResolver = require('./create-file-path-resolver')

const findDependencies = (absolutePathOfModule) => {
  const _content = fs.readFileSync(absolutePathOfModule, 'utf-8')
  const ast = babel.parseSync(_content)
  return ast.program.body
    .filter((node) => node.type === 'ImportDeclaration')
    .map((node) => node.source.value)
    .map((relativeFilePathOfDependency) =>
      path.join(
        path.dirname(absolutePathOfModule),
        relativeFilePathOfDependency,
      ),
    )
}

const createGraphNode = (path, dependencies) => ({
  filePath: path,
  dependencies,
  get content() {
    delete this.content
    const transformer = createTransformer(createFilePathResolver(this.filePath))
    this.content = transformer(fs.readFileSync(this.filePath), this.filePath)
    return this.content
  },
})

const createModuleDependencyGraphInternal = (graph, absolutePathOfModule) => {
  var dependencies = findDependencies(absolutePathOfModule)
  graph[absolutePathOfModule] = createGraphNode(
    absolutePathOfModule,
    dependencies,
  )
  dependencies.forEach((dependency) => {
    if (!(dependency in graph))
      createModuleDependencyGraphInternal(graph, dependency)
  })
}

const createModuleDependencyGraph = (
  absolutePathOfParentModule,
  relativeFilePath,
) => {
  var graph = {}
  createModuleDependencyGraphInternal(
    graph,
    path.join(absolutePathOfParentModule, relativeFilePath),
  )
  return graph
}

module.exports = createModuleDependencyGraph
