const createModuleDependencyGraph = require('./create-module-dependency-graph')
const createModuleMap = require('./create-module-map')
const createRuntime = require('./create-runtime')
const fs = require('fs')
const path = require('path')
const bundle = ({
  entryFile,
  entryFilePath,
  outputFolder,
  outputName = 'bundle.js',
}) => {
  const moduleDependencyGraph = createModuleDependencyGraph(
    entryFilePath,
    entryFile,
  )
  const moduleMap = createModuleMap(moduleDependencyGraph)
  const runtime = createRuntime(moduleMap, path.join(entryFilePath, entryFile))
  fs.writeFileSync(path.join(outputFolder, outputName), runtime, 'utf-8')
}

module.exports = bundle
