const collectModules = (module, modules = []) => {
  modules.push(module)
  module.dependencies.forEach((dependency) => {
    collectModules(dependency, modules)
  })
  return modules
}

const createModuleMap = (moduleDepedencyGraph) => {
  var moduleEntries = Object.keys(moduleDepedencyGraph)
    .map((moduleKey) => {
      var module = moduleDepedencyGraph[moduleKey]
      return `'${module.filePath}': function (exports,require){ ${module.content} }`
    })
    .join(',')
  return `{ ${moduleEntries} }`
}
module.exports = createModuleMap
