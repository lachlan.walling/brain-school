const http = require('http')
const server = http.createServer()
const requestListener = (request, response) => {
  if (request.method === 'GET') {
    const jsonData = JSON.stringify({
      studentName: 'Gerson Jepzir',
      hobbies: ['Yelling', 'Building miniature skyscrapers out of pop tart'],
    })
    response.statusCode = 200
    response.setHeader('Content-Type', 'application/json')
    response.write(jsonData)
    response.end()
  } else {
    response.statusCode = 405
    response.end()
  }
}

server.on('request', requestListener)

server.listen(8080)
