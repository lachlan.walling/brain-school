const express = require('express')
const mongoose = require('mongoose')
var bcrypt = require('bcryptjs')
const sessions = require('client-sessions')
const csurf = require('csurf')
const helmet = require('helmet')
const bodyParser = require('body-parser')

mongoose.connect('mongodb://db/ss-auth')

let User = mongoose.model(
  'User',
  new mongoose.Schema({
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
  }),
)

let app = express()

app.set('view engine', 'pug')

app.use(bodyParser.urlencoded({ extended: false }))

app.use(
  sessions({
    cookieName: 'session',
    secret: 'woosafd32532wfsf',
    duration: 30 * 60 * 1000,
    activeDuration: 5 * 60 * 1000,
    httpOnly: true,
    secure: true,
    ephemeral: true,
  }),
)

app.use(csurf())

app.use(helmet())

app.use(async (req, res, next) => {
  try {
    if (req.session && req.session.userId) {
      const user = await User.findById(req.session.userId)
      if (user) {
        user.pasword = undefined
        req.user = user
        res.locals.user = user
      }
    }
    next()
  } catch (error) {
    next(error)
  }
})

function loginRequired(req, res, next) {
  if (req.user) {
    next()
  } else {
    res.redirect('/login')
  }
}

app.get('/', (req, res) => {
  res.render('index')
})

app.get('/register', (req, res) => {
  res.render('register', { csrfToken: req.csrfToken() })
})

app.post('/register', async (req, res) => {
  try {
    let hash = bcrypt.hashSync(req.body.password, 14)
    req.body.password = hash
    let user = new User(req.body)

    await user.save()
    req.session.userId = user._id
    res.redirect('/dashboard')
  } catch (err) {
    let errorMessage = 'Something bad happened! please try again.'
    if (err.code === 11000) {
      errorMessage = 'That email is already taken, please try another.'
    } else {
      console.log(err)
    }

    res.render('register', { errorMessage })
  }
})

app.get('/login', (req, res) => {
  res.render('login', { csrfToken: req.csrfToken() })
})

app.post('/login', async (req, res) => {
  try {
    let user = await User.findOne({ email: req.body.email })
    if (bcrypt.compareSync(req.body.password, user.password)) {
      req.session.userId = user._id
      res.redirect('/dashboard')
    } else {
      res.render('login', {
        errorMessage: 'Incorrect email or pawssword',
      })
    }
  } catch (error) {
    res.render('login', {
      errorMessage: 'Something bad happened! please try again.',
    })
  }
})

app.get('/dashboard', loginRequired, (req, res) => {
  res.render('dashboard')
})

app.listen(3000)
