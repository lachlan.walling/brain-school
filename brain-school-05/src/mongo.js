const mongoose = require('mongoose')
const process = require('process')

let UserModel = null

async function connect() {
  const db = process.env.MONGO_INITDB_DATABASE
  const user = process.env.MONGO_USER
  const pass = process.env.MONGO_PASSWORD
  await mongoose.connect(`mongodb://${user}:${pass}@mongo:27017/${db}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
}

async function printUsers() {
  const users = await UserModel.find()
  const message = users
    .map((user) => `${user.firstName} ${user.lastName}`)
    .join(';')
  console.log(`${message}`)
}

async function clearDatabase() {
  await UserModel.deleteMany()
  mongoose.deleteModel(/.+/)
}

async function main() {
  console.log('Mongo with Mongoose')
  await connect()

  const userSchema = new mongoose.Schema({
    firstName: String,
    lastName: String,
  })

  UserModel = mongoose.model('User', userSchema)
  UserModel.deleteMany()

  const user1 = new UserModel()
  user1.firstName = 'Alan'
  user1.lastName = 'Bilby'
  await user1.save()

  const user2 = new UserModel()
  user2.firstName = 'Rami'
  user2.lastName = 'Ruhayel'
  await user2.save()

  console.log('Before delete')
  await printUsers()

  await UserModel.deleteOne({ firstName: 'Alan' })

  console.log('After delete')
  await printUsers()

  await clearDatabase()
}

module.exports = main
