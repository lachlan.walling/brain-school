const { DataSource, EntitySchema } = require('typeorm')
const process = require('process')

class User {
  constructor(id, firstName, lastName) {
    this.id = id
    this.firstName = firstName
    this.lastName = lastName
  }
}

var userRepository
var dataSource

async function connect() {
  dataSource = new DataSource({
    type: 'mysql',
    host: 'mysql',
    username: process.env['MYSQL_USER'],
    password: process.env['MYSQL_PASSWORD'],
    database: process.env['MYSQL_DATABASE'],
    logging: false,
    synchronize: true,
    entities: [
      new EntitySchema({
        target: User,
        name: 'User',
        columns: {
          id: {
            primary: true,
            generated: true,
          },
          firstName: {
            type: 'varchar',
          },
          lastName: {
            type: 'varchar',
          },
        },
      }),
    ],
  })
  await dataSource.initialize()
  userRepository = dataSource.getRepository('User')
}

async function disconnect() {}

async function clearDatabase() {
  await userRepository.createQueryBuilder().delete().execute()
}

async function printUsers() {
  //const users = await dataSource.createQueryBuilder().from(User).getMany()
  const users = await userRepository.find()
  const message = users
    .map((user) => `${user.firstName} ${user.lastName}`)
    .join(';')
  console.log(`${message}`)
}

async function main() {
  console.log('Mysql with TypeORM')

  await connect()
  await clearDatabase()

  const user1 = new User(0, 'Alan', 'Bilby')
  await userRepository.save(user1)

  const user2 = new User(1, 'Rami', 'Ruhayel')
  await userRepository.save(user2)

  console.log('Before delete')
  await printUsers()

  await userRepository
    .createQueryBuilder()
    .delete()
    .where('firstName = :firstName', { firstName: 'Alan' })
    .execute()

  console.log('After delete')
  await printUsers()

  await disconnect()
}

module.exports = main
