const modules = [
  require('./mongo'),
  require('./postgres'),
  require('./neo4j'),
  require('./mysql'),
]

async function main() {
  for (module of modules) {
    await module()
    console.log('')
  }

  function keepRunning() {
    setTimeout(keepRunning, 10000)
  }
  keepRunning()
}

main()
