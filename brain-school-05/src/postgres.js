const { Sequelize, DataTypes } = require('sequelize')
const process = require('process')

var sequalize
var User

async function connect() {
  sequalize = new Sequelize(
    process.env.POSTGRES_DB,
    process.env.POSTGRES_USER,
    process.env.POSTGRES_PASSWORD,
    {
      host: 'postgres',
      dialect: 'postgres',
      logging: false,
    },
  )
}

async function printUsers() {
  const users = await User.findAll()
  const message = users
    .map((user) => `${user.firstName} ${user.lastName}`)
    .join('; ')
  console.log(`${message}`)
}

async function clearDatabase() {
  await User.destroy({ where: {} })
}

async function main() {
  console.log('PostgreSQL with Sequalize')

  await connect()

  User = sequalize.define('User', {
    firstName: {
      type: DataTypes.STRING,
    },
    lastName: {
      type: DataTypes.STRING,
    },
  })

  await sequalize.sync({ force: true })

  const user1 = await User.create({
    firstName: 'Alan',
    lastName: 'Bilby',
  })
  const user2 = await User.create({
    firstName: 'Rami',
    lastName: 'Ruhayel',
  })

  console.log('Before delete')
  await printUsers()

  await User.destroy({
    where: {
      firstName: 'Alan',
    },
  })

  console.log('After delete')
  await printUsers()

  await clearDatabase()
}

module.exports = main
