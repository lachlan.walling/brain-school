const neo4j = require('neo4j-driver')
const process = require('process')

var driver
var session
async function connect() {
  driver = neo4j.driver(
    'neo4j://neo4j',
    neo4j.auth.basic(process.env['USER'], process.env['PASSWORD']),
  )
  session = driver.session()
}

async function disconnect() {
  await session.close()
  await driver.close()
}

class User {
  constructor(id, firstName, lastName) {
    this.id = id
    this.firstName = firstName
    this.lastName = lastName
  }
}

async function saveUser(user) {
  await session.run(
    'CREATE (user: User {id: $id, firstName: $firstName, lastName: $lastName}) RETURN user',
    user,
  )
}

async function deleteUser(firstName) {
  await session.run('MATCH (user: User {firstName: $firstName} ) DELETE user', {
    firstName,
  })
}

async function clearDatabase() {
  await session.run('MATCH (user: User ) DELETE user')
}

async function getUsers() {
  const result = await session.run('MATCH (user: User) RETURN user')
  return result.records.map((record) => record.get('user').properties)
}

async function printUsers() {
  const users = await getUsers()
  const message = users
    .map((user) => `${user.firstName} ${user.lastName}`)
    .join('; ')
  console.log(`${message}`)
}

async function main() {
  console.log('Neo4j with neo4j-driver')
  await connect()

  const user1 = new User(0, 'Alan', 'Bilby')
  await saveUser(user1)

  const user2 = new User(1, 'Rami', 'Ruhayel')
  await saveUser(user2)

  console.log('Before delete')
  await printUsers()

  await deleteUser('Alan')

  console.log('After delete')
  await printUsers()

  await clearDatabase()
  await disconnect()
}

module.exports = main
