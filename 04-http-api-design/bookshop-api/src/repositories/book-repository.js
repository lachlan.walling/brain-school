class BookRepository {
  async setup() {}
  async getAll() {}
  async getById(id) {}
  async save() {}
  async cleanup() {}
}

module.exports = BookRepository;
