const BookRepository = require("./book-repository");
const mongoose = require("mongoose");

class MongooseBookRepository extends BookRepository {
  async setup() {
    await mongoose.connect("mongodb://localhost/bookshop", {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    this.BookSchema = new mongoose.Schema({
      _id: String,
      title: String,
      author: String,
      price: Number,
    });

    this.BookModel = mongoose.model("Book", this.BookSchema);
  }

  async cleanup() {
    await this.BookModel.deleteMany({});
    mongoose.deleteModel(/.+/);

    await mongoose.connection.close();
  }

  convertFromMongo({ _id: id, title, author, price }) {
    return {
      id,
      title,
      author,
      price,
    };
  }

  async getAll() {
    const books = await this.BookModel.find();
    return books.map(this.convertFromMongo);
  }

  async getById(id) {
    const book = await this.BookModel.findById(id);
    if (book) {
      return this.convertFromMongo(book.toJSON());
    }
    return null;
  }

  async save({ id: _id, name, author, price }) {
    const mongoBook = new this.BookModel({
      _id,
      name,
      author,
      price,
    });
    mongoBook.save();
  }
}

module.exports = MongooseBookRepository;
