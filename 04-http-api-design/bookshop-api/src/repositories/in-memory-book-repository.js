const BookRepository = require("./book-repository");

class InMemoryBookRepository extends BookRepository {
  constructor() {
    super();
    this.books = new Map();
  }

  async save(book) {
    this.books.set(book.id, book);
  }

  async getAll() {
    let values = [];
    for (let value of this.books.values()) {
      values.push(value);
    }
    return values;
  }

  async getById(id) {
    return this.books.get(id);
  }
}

module.exports = InMemoryBookRepository;
