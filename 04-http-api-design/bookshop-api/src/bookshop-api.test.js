const request = require("supertest");
const App = require("./bookshop-api");
const Book = require("./book");
const InMemoryBookRepository = require("./repositories/in-memory-book-repository");
const { v4: uuidv4 } = require("uuid");

describe("bookshop-api", () => {
  let server;
  let bookRepository;

  beforeEach(async () => {
    bookRepository = new InMemoryBookRepository();
    server = new App(bookRepository);
    await server.start();
  });

  afterEach(async () => {
    await server.stop();
  });

  describe("GET /books", () => {
    let booksResponse;
    beforeEach(async () => {
      const book = new Book("Test book", "Test author", 9.99);
      await bookRepository.save(book);
      booksResponse = await request(server.app).get("/books");
    });

    it("responds with JSON", async () => {
      expect(booksResponse.status).toBe(200);
      expect(booksResponse.type).toBe("application/json");
    });

    it("returns a HAL resource with a self-link", () => {
      expect(booksResponse.body._links.self.href).toBe("/books");
    });

    it("includes all books in the resource as items with self-links", () => {
      expect(booksResponse.body.items.length).toBeGreaterThan(0);
      expect(booksResponse.body.items[0]._links.self.href).toBe(`/books/${booksResponse.body.items[0].id}`);
    });

    it("includes a create link", () => {
      expect(booksResponse.body._links.create.href).toBe("/books");
      expect(booksResponse.body._links.create.method).toBe("POST");
    });
  });

  describe("GET /books/:id", () => {
    let book;
    beforeEach(async () => {
      book = new Book("Test book", "Test author", 9.99);
      await bookRepository.save(book);
    });

    it("responds with JSON", async () => {
      const response = await request(server.app).get(`/books/${book.id}`);

      expect(response.status).toBe(200);
      expect(response.type).toBe("application/json");
    });

    it("returns a HAL resource with a self-link and a collection link", async () => {
      const response = await request(server.app).get(`/books/${book.id}`);

      expect(response.body._links.self.href).toBe(`/books/${book.id}`);
      expect(response.body._links.collection.href).toBe("/books");
    });

    it("returns a 404 if the book is not found", async () => {
      const response = await request(server.app).get(`/books/${uuidv4()}`);

      expect(response.status).toBe(404);
    });
  });

  describe("POST /books", () => {
    it("creates a new book", async () => {
      const book = { title: "Test book", author: "Test author", price: 9.99 };
      const response = await request(server.app).post("/books").send(book);

      expect(response.status).toBe(201);
      expect(response.type).toBe("application/json");
      expect(response.body.title).toBe(book.title);
      expect(response.body.author).toBe(book.author);
      expect(response.body.price).toBe(book.price);
    });
  });
});
