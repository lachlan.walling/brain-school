const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const halson = require("halson");
const Book = require("./book");

class App {
  constructor(bookRepository) {
    this.app = express();
    this.app.use(bodyParser.json());
    this.createRoutes();
    this.bookRepository = bookRepository;
  }

  createRoutes() {
    this.app.get("/books", async (req, res) => {
      const books = await this.bookRepository.getAll();
      const resource = halson({
        title: "Bookshop API",
        items: books.map((book) => {
          const bookResource = halson(book);
          bookResource.addLink("self", `/books/${book.id}`);
          return bookResource;
        }),
      });
      resource.addLink("self", "/books");
      resource.addLink("create", { href: "/books", method: "POST" });

      res.json(resource);
    });

    this.app.get("/books/:id", async (req, res) => {
      const book = await this.bookRepository.getById(req.params.id);

      if (!book) {
        res.sendStatus(404);
        return;
      }

      const resource = halson(book);
      resource.addLink("self", `/books/${book.id}`);
      resource.addLink("collection", "/books");

      res.json(resource);
    });

    this.app.post("/books", async (req, res) => {
      const book = new Book(req.body.title, req.body.author, req.body.price);
      await this.bookRepository.save(book);

      const resource = halson(book);
      resource.addLink("self", `/books/${book.id}`);
      resource.addLink("collection", "/books");

      res.status(201).json(resource);
    });
  }

  start() {
    return new Promise((resolve) => {
      this.server = this.app.listen(3000, async () => {
        await this.bookRepository.setup();
        resolve();
      });
    });
  }

  stop() {
    return new Promise(async (resolve) => {
      this.server.close(async () => {
        await this.bookRepository.cleanup();
        resolve();
      });
    });
  }
}

module.exports = App;
