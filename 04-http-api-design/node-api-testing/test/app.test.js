const request = require("supertest");
const App = require("../app");
const { expect } = require("chai");

describe("GET /", () => {
  var app;
  beforeEach(async () => {
    app = new App();
    await app.start();
  });
  afterEach(async () => {
    await app.stop();
  });
  it('should respond with "Hello, World!"', async () => {
    const res = await request(app.app).get("/");
    expect(res.status).to.equal(200);
    expect(res.text).to.equal("Hello, World!");
  });

  it("should create a new resource", async () => {
    const res = await request(app.app).post("/resource").send({ name: "New resource" });
    expect(res.status).to.equal(201);
    expect(res.body).to.have.property("id");
    expect(res.body.name).to.equal("New resource");
  });

  it("should return a 400 error for invalid input", async () => {
    const res = await request(app.app).post("/resource").send({ wrongKey: "Wrong value" });
    expect(res.status).to.equal(400);
    expect(res.body).to.have.property("error");
  });

  it("should handle a get request with a parameter", async () => {
    const res = await request(app.app).get("/search").query({ name: "test" });
    expect(res.status).to.equal(200);
    expect(res.body).to.deep.equal([{ id: "1", name: "test" }]);
  });

  it("should handle a delete request", async () => {
    const res = await request(app.app).delete("/resource/1");
    expect(res.status).to.equal(204);
  });

  it("should habdle put request", async () => {
    const res = await request(app.app).put("/resource/1").send({ name: "New name" });
    expect(res.status).to.equal(200);
    expect(res.body).to.deep.equal({ id: "1", name: "New name" });
  });
});
