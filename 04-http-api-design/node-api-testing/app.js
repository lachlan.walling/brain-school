const express = require("express");

class App {
  constructor() {
    this.app = express();
    this.server = null;

    this.app.use(express.json());

    this.app.get("/", (req, res) => {
      res.status(200).send("Hello, World!");
    });

    this.app.post("/resource", (req, res) => {
      if (!("name" in req.body)) {
        return res.status(400).send({ error: "Missing name" });
      }
      const resource = { id: 1, name: req.body.name };
      res.status(201).send(resource);
    });

    this.app.get("/search", (req, res) => {
      if (req.query.name === "test") {
        return res.status(200).send([{ id: "1", name: "test" }]);
      }
      return res.status(200).send([]);
    });

    this.app.delete("/resource/:id", (req, res) => {
      return res.status(204).send();
    });

    this.app.put("/resource/:id", (req, res) => {
      const { id } = req.params;
      return res.status(200).send({ id, name: req.body.name });
    });
  }

  start() {
    return new Promise((resolve, reject) => {
      this.server = this.app.listen(3000, () => {
        resolve();
      });
    });
  }

  stop() {
    return new Promise((resolve, reject) => {
      try {
        this.server.close(() => {
          resolve();
        });
      } catch (err) {
        console.log(`error stopping: ${err}`);
        reject(err);
      }
    });
  }
}

module.exports = App;
//module.exports.stop = stop;
